# This file requires a blank line at the end.

dbLoadDatabase("./tCat.dbd",0,0)
tCat_registerRecordDeviceDriver(pdbbase)
callbackSetQueueSize(10000)

tcSetScanRate(10, 5)
tcGenerateList ("C:\SlowControls\TwinCAT3\Sandbox\Target\H1HPIPUMPCTRLL0\h1hpipumpctrll0.txt", "-rv -l")
tcGenerateList ("C:\SlowControls\TwinCAT3\Sandbox\Target\H1HPIPUMPCTRLL0\h1hpipumpctrll0.req", "-rv -lb")
tcGenerateList ("C:\SlowControls\TwinCAT3\Sandbox\Target\H1HPIPUMPCTRLL0\h1hpipumpctrll0.ini", "-rv -l -ns")
tcLoadRecords ("C:\SlowControls\TwinCAT3\Sandbox\Target\H1HPIPUMPCTRLL0\H1HPIPUMPCTRLL0\PLC1\PLC1.tpy", "-rv")

iocInit()
